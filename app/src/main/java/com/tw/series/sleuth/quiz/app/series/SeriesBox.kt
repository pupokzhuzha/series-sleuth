package com.tw.series.sleuth.quiz.app.series

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.tw.series.sleuth.quiz.app.R
import com.tw.series.sleuth.quiz.app.another.SeriesInfo
import com.tw.series.sleuth.quiz.app.another.SeriesObj
import com.tw.series.sleuth.quiz.app.databinding.FragmentSeriesBoxBinding
import eightbitlab.com.blurview.BlurView
import eightbitlab.com.blurview.RenderScriptBlur

class SeriesBox : Fragment() {

    private var binding: FragmentSeriesBoxBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private lateinit var model : SeriesInfo
    private var currentAnswer = ""
    private var points = 0

    private lateinit var mBlurView: BlurView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSeriesBoxBinding.inflate(layoutInflater, container, false)
        mBinding.apply {
            mBlurView = blur
        }
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = SeriesObj.gen(page)

        mBinding.apply {
            image.setImageResource(model.image)
            tittle.text = model.tittle
            firstText.text = model.first
            secondText.text = model.second
            thirdText.text = model.third
            currentAnswer = model.rightAnswer
            next.isEnabled = false

            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            first.setOnClickListener {
                next.isEnabled = true
                if(firstText.text == currentAnswer){
                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
                    points++
                }
                else{
                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
                }

                first.isEnabled = false
                second.isEnabled = false
                third.isEnabled = false
            }
            second.setOnClickListener {
                next.isEnabled = true
                if(secondText.text == currentAnswer){
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
                    points++
                }
                else{
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
                }

                first.isEnabled = false
                second.isEnabled = false
                third.isEnabled = false
            }
            third.setOnClickListener {
                next.isEnabled = true
                if(thirdText.text == currentAnswer){
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
                    points++
                }
                else{
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
                }

                first.isEnabled = false
                second.isEnabled = false
                third.isEnabled = false
            }
            next.setOnClickListener {
                next.isEnabled = false
                page++
                if (page == 10){
                    val radius = 8f
                    val decorView = activity?.window?.decorView
                    val rootView = decorView?.findViewById<ViewGroup>(android.R.id.content)
                    val windowBackground = decorView?.background
                    if (rootView != null) {
                        mBlurView.setupWith(rootView, RenderScriptBlur(requireContext()))
                            .setFrameClearDrawable(windowBackground)
                            .setBlurRadius(radius)
                    }
                    mBlurView.visibility = View.VISIBLE

                    back.isEnabled = false
                    result.text = "$points/10"
                }
                else{
                    model = SeriesObj.gen(page)

                    first.isEnabled = true
                    second.isEnabled = true
                    third.isEnabled = true

                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)

                    image.setImageResource(model.image)
                    tittle.text = model.tittle
                    firstText.text = model.first
                    secondText.text = model.second
                    thirdText.text = model.third
                    currentAnswer = model.rightAnswer
                }
            }
            toMenu.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
package com.tw.series.sleuth.quiz.app.series

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.tw.series.sleuth.quiz.app.R
import com.tw.series.sleuth.quiz.app.databinding.FragmentLoadBoxBinding

class LoadBox : Fragment() {

    private var binding: FragmentLoadBoxBinding? = null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoadBoxBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            start.setOnClickListener {
                findNavController().navigate(R.id.action_loadBox_to_seriesBox)
            }
            exit.setOnClickListener {
                activity?.finishAndRemoveTask()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
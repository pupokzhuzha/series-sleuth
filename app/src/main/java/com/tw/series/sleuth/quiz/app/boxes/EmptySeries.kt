package com.tw.series.sleuth.quiz.app.boxes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.tw.series.sleuth.quiz.app.R
import com.tw.series.sleuth.quiz.app.another.replaceActivity

class EmptySeries : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty_series)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(Box())
        }, 500)
    }
}
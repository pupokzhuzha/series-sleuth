package com.tw.series.sleuth.quiz.app.another

data class SeriesInfo(
    val image : Int,
    val tittle : String,
    val first : String,
    val second : String,
    val third : String,
    val rightAnswer : String
)

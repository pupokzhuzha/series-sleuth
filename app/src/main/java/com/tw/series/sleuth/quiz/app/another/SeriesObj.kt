package com.tw.series.sleuth.quiz.app.another

import com.tw.series.sleuth.quiz.app.R

object SeriesObj {
    fun gen(page : Int) : SeriesInfo{
        return when(page){
            1 -> SeriesInfo(R.drawable.blinders, "Name this series.", "Peaky Blinders", "Boardwalk Empire", "Luther", "Peaky Blinders")
            2 -> SeriesInfo(R.drawable.maisel, "What is the name of this comedy-drama series from Amazon?", "The lives of a matryoshka", "Fleabag", "The Marvelous Mrs. Maisel", "The Marvelous Mrs. Maisel")
            3 -> SeriesInfo(R.drawable.things, "What is the name of this hit Netflix series?", "Stranger things", "Chilling Adventures of Sabrina", "Riverdalse", "Stranger things")
            4 -> SeriesInfo(R.drawable.crown, "Name this series.", "Bridgertons", "Crown", "Downton Abbey", "Crown")
            5 -> SeriesInfo(R.drawable.doctor, "Are you familiar with this series?", "Good Doctor", "New Amsterdam", "Grey's Anatomy", "Good Doctor")
            6 -> SeriesInfo(R.drawable.you, "Are you familiar with this series?", "Prodigal son", "Riverdale", "You", "You")
            7 -> SeriesInfo(R.drawable.method, "Can you name this series?", "Grace and Frankie", "Barry", "Kominsky method", "Kominsky method")
            8 -> SeriesInfo(R.drawable.house, "Are you familiar with this horror series?", "Chilling Adventures of Sabrina", "Midnight Mass", "The Haunting of Hill House", "The Haunting of Hill House")
            else -> SeriesInfo(R.drawable.narcos, "Name this series.", "Narcos", "El Chapo", "Pablo Escobar, master of evil", "Narcos")
        }
    }
}